<!--
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-10 17:36:32
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-10-08 14:16:02
 * @FilePath: \garden_web\README.md
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
-->
> 源码地址：[https://github.com/Mstian/Vue-Onepiece-Admin](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2FMstian%2FVue-Onepiece-Admin)

# Vue3.0 ElementPlus

## Project setup
```
npm install
```

## Start Mock Server
```
npm run mock
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
