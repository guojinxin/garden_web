import * as axios from "axios";
import * as EventEmitter from 'events';
import { useRouter } from 'vue-router';

/**
 * 提示函数
 * 禁止点击蒙层、显示一秒后关闭
 */
const tip = msg => {
  alert(msg);
};


class Request extends EventEmitter {
  constructor() {
    super();
    this.interceptors();
  }

  interceptors() {
    // 设置post请求头
    // axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8';
    // axios.defaults.headers.post['Access-Control-Allow-Credentials'] = true;
    axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
    // axios.defaults.headers.post['Access-Control-Allow-Methods'] = 'POST';
    // axios.defaults.headers.post['Access-Control-Allow-Headers'] = 'x-requested-with,content-type';

    // // 响应类型
    // header('Access-Control-Allow-Methods:POST');
    // // 响应头设置
    // header('Access-Control-Allow-Headers:x-requested-with,content-type');
    /**
     * 请求拦截器
     * 每次请求前，如果存在token则在请求头中携带token
     */
    axios.defaults.withCredentials = true; //让ajax携带cookie(自动携带本地所有cookie)
    axios.interceptors.request.use(
      config => {
        // 登录流程控制中，根据本地是否存在token判断用户的登录情况
        // 但是即使token存在，也有可能token是过期的，所以在每次的请求头中携带token
        // 后台根据携带的token判断用户的登录情况，并返回给我们对应的状态码
        // 而后我们可以在响应拦截器中，根据状态码进行一些统一的操作。
        const token = localStorage.token;
        token && (config.headers.authorization = token);
        config.headers.credentials = true;
        return config;
      },
      error => Promise.error(error));
    // 响应拦截器
    axios.interceptors.response.use(
      // 请求成功
      response => {
        return response.status === 200 ? Promise.resolve(response) : Promise.reject(response);
      },
      // 请求失败
      error => {
        const {
          response
        } = error;
        if (response) {
          const router = useRouter();

          /**
           * 请求失败后的错误统一处理
           * @param {Number} status 请求失败的状态码
           */
          const errorHandle = (status, other) => {
            // 状态码判断
            switch (status) {
              // 401: 未登录状态，跳转登录页
              case 401:
                tip('此页面只能管理员查看');
                localStorage.clear();
                //   store.commit('loginSuccess', null);
                setTimeout(() => {
                  window.location.href = '/#/login';
                }, 1000);
                break;
              // 403 token过期
              // 清除token并跳转登录页
              case 403:
                tip('登录过期，请重新登录');
                localStorage.clear();
                //   store.commit('loginSuccess', null);
                setTimeout(() => {
                  window.location.href = '/#/login';
                }, 1000);
                break;
              // 404请求不存在
              case 404:
                tip('请求的资源不存在');
                break;
              default:
                console.log(other);
            }
          };

          // 请求已发出，但是不在2xx的范围
          errorHandle(response.status, response.data.message);
          return Promise.reject(response);
        } else {

          // 处理断网的情况
          // eg:请求超时或断网时，更新state的network状态
          // network状态在app.vue中控制着一个全局的断网提示组件的显示隐藏
          // 关于断网组件中的刷新重新获取数据，会在断网组件中说明
          //   store.commit('changeNetwork', false);
        }
      });

    axios.interceptors.request.use(
      config => {
        return config;
      },
      error => {
        return Promise.reject(error);
      }
    );
    axios.interceptors.response.use(
      response => {
        const code = response.status;
        if ((code >= 200 && code < 300) || code === 304) {
          this.emit("HttpStatusSuccess");
          // response.data
          return Promise.resolve(response.data);
        } else {
          // 响应错误逻辑处理 5xx 4xx 等等
          this.emit("HttpStatusFaild");
          return Promise.reject(response);
        }
      },
      error => {
        // 响应错误逻辑处理
        // const code = response.status;
        this.emit("HttpStatusFaild");
        return Promise.reject(error);
      }
    );
  }

  get(url, params) {
    return axios({
      method: 'get',
      url: process.env.VUE_APP_PC_URL + url,
      params, withCredentials: true
    });
  }
  post(url, data) {
    return axios({
      method: 'post',
      url: process.env.VUE_APP_PC_URL + url,
      // withCredentials: true,
      data, withCredentials: true
    });
  }
  delete(url, data) {
    return axios({
      method: 'delete',
      url: process.env.VUE_APP_PC_URL + url,
      data
    });
  }
  put(url, data) {
    return axios({
      method: 'put',
      url: process.env.VUE_APP_PC_URL + url,
      data
    });
  }
  patch(url, data) {
    return axios({
      method: 'patch',
      url: process.env.VUE_APP_PC_URL + url,
      data
    });
  }
}

let request = new Request();

export default request;

