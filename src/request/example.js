/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-10 13:24:23
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-08-10 14:00:46
 * @FilePath: \Vue-Onepiece-Admin-master\src\request\example.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from './index';
// import request from "./index";

request.get('http://localhost:3001/api/wans').then((data) => {
    console.log(data, 'datasssss');
});

let params = {
    id: '101',
    IPStatus: 'CONNECTING',
    currentIP: '192.168.2.1',
    currentMask: '255.255.255.0',
    currentGateway: '192.168.2.1',
    currentDNS: '192.168.2.1',
    enable: true,
    IPMode: 'DHCP',
    connectionTrigger: 'auto',
    keepAliveTime: '1608188204153',
    address: '192.168.2.1',
    mask: '255.255.255.0',
    gateway: '192.168.2.1',
    staticDns: true,
    DNS: '192.168.2.1',
    username: 'tianleilei',
    password: '11000000'
};
// request.post('http://localhost:3000/api/wans', params).then((data) => {
//     console.log(data, 'datasssss');
// })

request.on('HttpStatusSuccess', () => {
    // 这里进行自定义弹窗提示
    console.log("现在是请求成功了");
});
request.on('HttpStatusCodeError', () => {
    // 这里进行自定义弹窗错误提示
    console.log("现在是请求失败了");
});