/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2023-01-29 16:55:24
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-02-01 14:43:28
 * @FilePath: \garden_web\src\store\visitInfo\index.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import _ from 'lodash';
export default {
  state: {
    isVisitLoading: false,
    visitList: []
  },
  mutations: {
    visitInfo(state: any, payload: any) {
      state.isVisitLoading = !(state.isCollapse);
      state.visitList.push(payload);
    },
    removeVisitFun(state: any, payload: any) {
      const data = _.filter(state.visitList, function (item: any) {
        return item.id !== payload;
      });
      state.visitList = data;
    },
    cavitationVisitFun(state: any) {
      state.visitList = [];
    },
    initialVisitFun(state: any, payload: any) {
      state.visitList = payload;
    },
    updateVisitFun(state: any, payload: any) {
      state.visitList.forEach((element: { id: any }, index: number) => {
        if (element.id === payload.id) {
          state.visitList[index] = payload;
        }
      });
    },
  },
  actions: {
    getVisitInfo(context: any, payload: any) {
      context.commit('visitInfo', payload);
    },
    removeVisitData(context: any, payload: any) {
      context.commit('removeVisitFun', payload);
    },
    cavitationVisitData(context: any) {
      context.commit('cavitationVisitFun');
    },
    updateVisitData(context: any, payload: any) {
      context.commit('updateVisitFun', payload);
    },
    initialVisitData(context: any, payload: any) {
      context.commit('initialVisitFun', payload);
    },
  },
  getters: {
  },
  modules: {
  }
};
