/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-10 17:40:50
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-01-29 16:56:41
 * @FilePath: \garden_web\src\store\index.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { createStore } from 'vuex';
import controls from './controls';
import visitInfo from './visitInfo';

export default createStore({
  modules: {
    controls,
    visitInfo
  }
});
