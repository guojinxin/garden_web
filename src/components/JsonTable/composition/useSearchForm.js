/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-10 17:40:50
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-03-22 18:12:55
 * @FilePath: \garden_web\src\components\JsonTable\composition\useSearchForm.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { reactive, ref, nextTick } from "vue";
import dayjs from "dayjs";
import emitter from "@/utils/mitt.js";
import _ from "lodash";
export function useInitSearchForm(props) {
  let searchForm = reactive({}); // 初始化表单项
  const isLoading = ref(false); // 控制loading
  const tableData = ref([]); // 表格数据
  props.searchColumns.forEach((item, index) => {
    searchForm[item.prop] = "";
  });
  let pagination = reactive({
    // 分页相关
    page: 1,
    psize: 20,
    total: 0,
  });
  let formatDate = (val, searchForm, prop) => {
    // 格式化日期
    if (Array.isArray(val)) {
      let temp = [
        dayjs(val[0]).format("YYYY-MM-DD HH:mm:ss"),
        dayjs(val[1]).format("YYYY-MM-DD HH:mm:ss"),
      ];
      searchForm[prop] = temp;
    } else {
      searchForm[prop] = dayjs(val).format("YYYY-MM-DD");
    }
  };

  let handleSubmit = () => {
    // 提交
    isLoading.value = true;
    if (!_.isEmpty(searchForm.id)) {
        searchForm.id = decodeURI(searchForm.id);
    }
    let temp = {
      page: pagination.page,
      psize: pagination.psize,
      params: searchForm,
    };
    props.service.get(temp).then((res) => {
      if (res.code === 0) {
        isLoading.value = false; // 控制loading
        tableData.value = res.data.rows;
        emitter.emit("tableData", res.data.rows);

        pagination.total = res.data.count;
      }
    });
  };

  let caculateTableHeight = (tableHeight, tableRef) => {
    nextTick().then(() => {
      let total =
        tableRef.value.$el.offsetTop +
        tableRef.value.$el.offsetParent.offsetTop +
        tableRef.value.$el.nextElementSibling.offsetHeight +
        20;
      tableHeight.value = window.innerHeight - total;
    });
  };
  return {
    searchForm,
    formatDate,
    handleSubmit,
    tableData,
    caculateTableHeight,
    pagination,
    isLoading,
    modifyData: props.modifyData,
  };
}
