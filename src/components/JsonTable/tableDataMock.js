/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-10 17:40:50
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-08-25 09:49:38
 * @FilePath: \garden_web\src\components\JsonTable\tableDataMock.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// 模拟的表格数据
export const tableData = {
    "code": 0,
    "data": [
        {
            "name": "Kristy Emard",
            "idNum": 370112199902167412,
            "visitTime": "2022-08-16 00:00:00",
            "licensePlateNum": "鲁A46S78",
            "auditUser": "West Aronfurt",
            "address": '服务楼',
            "healthCode": "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3166639134,1440270722&fm=26&gp=0.jpg",
            "tourCode": "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3166639134,1440270722&fm=26&gp=0.jpg",
            "createUser": "tianleilei",
            "describe": '我今天想去看看华润产业园',
            "mobile": '15169165788',
            "jobNumber": 'gwx9946320',
            "status": 1,
            "children": [
                {

                    "name": "Kristy Emard",
                    "idNum": 37011218546213267412,
                    "visitTime": "2022-08-16 00:00:00",
                    "licensePlateNum": "鲁A46S78",
                    "auditUser": "West Aronfurt",
                    "address": '服务楼',
                    "healthCode": "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3166639134,1440270722&fm=26&gp=0.jpg",
                    "tourCode": "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3166639134,1440270722&fm=26&gp=0.jpg",
                    "createUser": "tianleilei",
                    "describe": '我今天想去看看华润产业园',
                    "mobile": '15169165788',
                    "jobNumber": 'gwx9946320',
                    "status": 1,

                }
            ],
        },
        {
            "name": "jelly.Kim",
            "idNum": 37014556217869841,
            "visitTime": "2022-10-16 24:24:24",
            "licensePlateNum": "鲁A448X8",
            "auditUser": "郭先生",
            "address": '综合楼',
            "healthCode": "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3166639134,1440270722&fm=26&gp=0.jpg",
            "tourCode": "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3166639134,1440270722&fm=26&gp=0.jpg",
            "createUser": "李信",
            "describe": '我今天想去看看华润产业园',
            "mobile": '88888888',
            "jobNumber": 'gwx9946320',
            "status": 0
        },
        {
            "name": "Kristy Emard",
            "idNum": 370112199956465412,
            "visitTime": "2022-08-16 00:00:00",
            "licensePlateNum": "鲁A46S78",
            "auditUser": "West Aronfurt",
            "address": '服务楼',
            "healthCode": "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3166639134,1440270722&fm=26&gp=0.jpg",
            "tourCode": "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3166639134,1440270722&fm=26&gp=0.jpg",
            "createUser": "tianleilei",
            "describe": '我今天想去看看华润产业园',
            "mobile": '15169165788',
            "jobNumber": 'gwx9946320',
            "status": 1
        },
    ]
};
// 模拟的表格数据
export const useTableData = {
    "code": 0,
    "data": [
        {
            "name": "Kristy Emard",
            "gender": '1',
            "phone": '15169165788',
            "idNum": 370112199902167412,
            "jobNum": 'gwx9946320',
            "birthDate": "2022-08-16 00:00:00",
            "licensePlateNum": "鲁A46S78",
            "workplace": '服务楼',
            "healthCode": "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3166639134,1440270722&fm=26&gp=0.jpg",
            "isApproval": true
        },
        {
            "name": "Kristy Emard",
            "gender": '1',
            "phone": '15169165788',
            "idNum": 370112199902167412,
            "jobNum": 'gwx9946320',
            "birthDate": "2022-08-16 00:00:00",
            "licensePlateNum": "鲁A46S78",
            "workplace": '服务楼',
            "healthCode": "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3166639134,1440270722&fm=26&gp=0.jpg",
            "isApproval": true
        },
        {
            "name": "Kristy Emard",
            "gender": '1',
            "phone": '15169165788',
            "idNum": 370112199902167412,
            "jobNum": 'gwx9946320',
            "birthDate": "2022-08-16 00:00:00",
            "licensePlateNum": "鲁A46S78",
            "workplace": '服务楼',
            "healthCode": "https://dss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3166639134,1440270722&fm=26&gp=0.jpg",
            "isApproval": true
        }
    ]
};