/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-07 10:03:05
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-03-28 10:45:07
 * @FilePath: \garden_web\src\components\JsonTable\useWaterConfig.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from '@/request';
import moment from 'moment';
import _ from 'lodash';
// import { tableData } from './tableDataMock';
export const searchColumns = [
  {
    label: '表号',
    prop: 'name',
    clearable: true,
    placeholder: "请输入表号"
  },
  {
    label: '统计日期',
    prop: 'entry_time',
    clearable: true,
    placeholder: "选择统计日期",
    isTime: 'datetimerange',
    type: 'monthrange',
    // format: 'YYYY-MM'
  }
];

export const tableColumns = [
  {
    prop: 'entry_time',
    label: '数据日期',
    overflow: true
  },
  {
    prop: 'created_time',
    label: '数据统计日期',
    overflow: true
  },
  {
    prop: 'name',
    label: '表号',
    overflow: true,
  },
  {
    prop: 'machineData',
    label: '机表数据',
    overflow: true,
  },
  {
    prop: 'position',
    label: '位置',
    overflow: true,
  },
  {
    prop: 'isAutomatic',
    label: '是否自动采集',
    expandFunc: true,
    isImage: true,
    render: (scope) => {
      return scope.row.isAutomatic ? require('../../assets/isAutomatic.svg') : '-';
    }
  }
];

export const localService = {
  /**
   * {
   *  page: 1,
   *  psize: 20,
   *  params: {}
   * }
   */
  get(data) {
    if (isNaN(data.params.entry_time[0]) && !isNaN(Date.parse(data.params.entry_time[0]))) {
      data.params.entryTimeStr = data.params.entry_time[0];
      data.params.entryTimeEnd = data.params.entry_time[1];
      // delete data.params.dateVisit;
    } else {
      delete data.params.entryTimeStr;
      delete data.params.entryTimeEnd;
    }
    // eslint-disable-next-line @typescript-eslint/camelcase
    return request.get("/useSteam/index", { page_no: data.page, page_size: data.psize, ...data.params }); // 这里是实际发请求的地方
    // return new Promise((resolve, reject) => {
    //     setTimeout(() => {
    //         resolve(tableData);
    //     }, 1000);
    // });
  }
};

export const options = {
  canCheck: false, // 是否可选择
  hasIndex: true, // 是否有序号
  checkFixed: 'left', // 选择固定位置
  indexFixed: 'left', // 表序号固定位置
  opW: 200,// 操作栏宽度
  autoRequest: true, // 自动请求
  startUpdate: Date.now()
};

// 以上配置文件可以根据业务需要分布配置在不同的文件里
