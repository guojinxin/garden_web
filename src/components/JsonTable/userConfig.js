import request from '@/request';
export const searchColumns = [
  {
    label: '姓名',
    prop: 'name',
    clearable: true,
    placeholder: "请输入姓名",
  },
  {
    label: '性别',
    prop: 'gender',
    clearable: true,
    placeholder: "性别",
    isSelect: true,
    options: [
      {
        prop: '0',
        name: '男'
      },
      {
        prop: '1',
        name: '女'
      }
    ]
  },
  {
    label: '手机号',
    prop: 'phone',
    clearable: true,
    placeholder: "请输入手机号"
  },
  {
    label: '部门',
    prop: 'department',
    clearable: true,
    placeholder: "请输入部门"
  },
  {
    label: '账号',
    prop: 'nickname',
    clearable: true,
    placeholder: "请输入账号"
  },
  {
    label: '角色',
    prop: 'role',
    clearable: true,
    placeholder: "角色",
    isSelect: true,
    options: [
      {
        prop: '0',
        name: '普通用户',
      },
      {
        prop: '1',
        name: '审批用户',
      },
      {
        prop: '2',
        name: '管理员',
      }
    ]
  },
  {
    label: '告警类型',
    prop: 'alarmPerson',
    clearable: true,
    placeholder: "告警类型",
    isSelect: true,
    options: [
      {
        prop: '1',
        name: '污水检测指标告警负责人',
      },
      {
        prop: '2',
        name: '水电处理人',
      },
      {
        prop: '3',
        name: '室内温度指标告警负责人',
      },
      {
        prop: '4',
        name: '消防告警负责人',
      },
      {
        prop: '5',
        name: '门岗告警负责人',
      },
      {
        prop: '6',
        name: '温湿度告警负责人',
      },
      {
        prop: '7',
        name: '门禁告警负责人',
      },
      {
        prop: '8',
        name: '监控告警负责人',
      }
    ]
  },
];
export const tableColumns = [
  {
    prop: 'name',
    label: '姓名',
    overflow: true
  },
  {
    prop: 'gender',
    label: '性别',
    overflow: true,
    isMultiCell: true,
    expandFunc: true,
    render: (scope) => {
      let status = scope.row.gender;
      if (status === "0") {
        return "男";
      } else if (status === "1") {
        return "女";
      }
      return "--";
    }
  },
  {
    prop: 'email',
    label: '邮箱',
    overflow: true
  },
  {
    prop: 'phone',
    label: '手机号',
    overflow: true
  },
  {
    prop: 'jobNum',
    label: '工号',
    overflow: true
  },
  {
    prop: 'department',
    label: '部门',
    overflow: true
  },
  {
    prop: 'alarmPerson',
    label: '告警类型',
    overflow: true,
    expandFunc: true,
    isTag: true,
    render: (scope,alarmPersonItem) => {
      const status = alarmPersonItem;
      if (!scope.row.isAlarm) {
        return "--";
      }
      if (status === "1") {
        return "污水检测指标告警负责人";
      } else if (status === "2") {
        return "水电处理人";
      } else if (status === "3") {
        return '室内温度指标告警负责人';
      } else if (status === "4") {
        return '消防告警负责人';
      } else if (status === "5") {
        return '门岗告警负责人';
      } else if (status === '6') {
        return '温湿度告警负责人';
      } else if (status === '7') {
        return '门禁告警负责人';
      } else if (status === '8') {
        return '监控告警负责人';
      }
      return "--";
    }
  },
  {
    prop: 'isApproval',
    label: '审批权限',
    isMultiCell: true,
    overflow: false,
    expandFunc: true,
    isApproval: true,
  },
  {   // 场景： 后端字段是数字0或1, 前端需要自己将数字转成汉字 比如 0普通用户 1审批用户 2管理员
    prop: 'role',
    label: '角色',
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      let status = scope.row.role;
      if (status === "0") {
        return "普通用户";
      } else if (status === "1") {
        return "审批用户";
      } else if (status === "2") {
        return '管理员';
      } else if (status === "3") {
        return '安保人员';
      }
      return "--";
    }
  }
];

export const localService = {
  /**
   * {
   *  page: 1,
   *  psize: 20,
   *  params: {}
   * }
   */
  async get(data) {
    // eslint-disable-next-line @typescript-eslint/camelcase
    return await request.get("users/index", { page_no: data.page, page_size: data.psize, ...data.params }); // 这里是实际发请求的地方

  }
};

export const options = {
  canCheck: false, // 是否可选择
  hasIndex: true, // 是否有序号
  checkFixed: 'left', // 选择固定位置
  indexFixed: 'left', // 表序号固定位
  opW: 200,// 操作栏宽度
  autoRequest: true, // 自动请求
  startUpdate: Date.now()
};

// 以上配置文件可以根据业务需要分布配置在不同的文件里
