/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-07 10:03:05
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-08 14:02:23
 * @FilePath: \garden_web\src\components\JsonTable\useWaterConfig.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from "@/request";
import _ from "lodash";
// import { tableData } from './tableDataMock';
export const searchColumns = [
  {
    label: "人员姓名",
    prop: "Name",
    clearable: true,
    placeholder: "请输入人员姓名",
  },
  {
    label: "时间段",
    prop: "CreateDateTime",
    clearable: true,
    placeholder: "选择时间段",
    isTime: "datetimerange",
  },
  {
    label: "设备位置",
    prop: "DevLocation",
    clearable: true,
    placeholder: "请输入设备位置",
  },
  {
    label: "部门",
    prop: "departmentName",
    clearable: true,
    placeholder: "请选择部门",
    isSelect: true,
    options: [
      {
        prop: "0",
        name: "研发部",
      },
    ],
  },
];

export const tableColumns = [
  {
    prop: "departmentName",
    label: "部门",
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      return scope.row.personnel?.objectinfo?.Name || "--";
    },
  },
  {
    prop: "user_name",
    label: "人员姓名",
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      return scope.row.personnel?.Name || "--";
    },
  },
  {
    prop: "DevLocation",
    label: "设备位置",
    overflow: true,
  },
  {
    prop: "CreateDateTime",
    label: "创建时间",
    overflow: true,
  },
];

export const localService = {
  /**
   * {
   *  page: 1,
   *  psize: 20,
   *  params: {}
   * }
   */
  get(data) {
    if (!_.isEmpty(data.params.CreateDateTime)) {

      // eslint-disable-next-line no-self-assign
      data.params.createTimeStr = data.params.CreateDateTime[0];
      // eslint-disable-next-line no-self-assign
      data.params.createTimeEnd = data.params.CreateDateTime[1];
      // delete data.params.dateVisit;
    } else {

      if (data.params.createTimeStr) {
        delete data.params.createTimeStr;
        delete data.params.createTimeEnd;
      }
    }

    // eslint-disable-next-line @typescript-eslint/camelcase
    return request.get("/entranceGuard/getDepartmentList", {page_no: data.page, page_size: data.psize,
      ...data.params,
    }); // 这里是实际发请求的地方
    // return new Promise((resolve, reject) => {
    //     setTimeout(() => {
    //         resolve(tableData);
    //     }, 1000);
    // });
  },
};

export const options = {
  canCheck: false, // 是否可选择
  hasIndex: true, // 是否有序号
  checkFixed: "left", // 选择固定位置
  indexFixed: "left", // 表序号固定位置
  opW: 200, // 操作栏宽度
  autoRequest: true, // 自动请求
  startUpdate: Date.now(),
};

// 以上配置文件可以根据业务需要分布配置在不同的文件里
