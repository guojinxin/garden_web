import request from '@/request';
export const searchColumns = [
  {
    label: '类型',
    prop: 'type',
    clearable: true,
    placeholder: "类型",
    isSelect: true,
    options: [
      {
        prop: '0',
        name: '水表'
      },
      {
        prop: '1',
        name: '电表'
      },
      {
        prop: '2',
        name: '汽表'
      }
    ]
  },
  {
    label: '状态',
    prop: 'status',
    clearable: true,
    placeholder: "状态",
    isSelect: true,
    options: [
      {
        prop: '0',
        name: '关闭'
      },
      {
        prop: '1',
        name: '开启'
      }
    ]
  },
  {
    label: '数据名称',
    prop: 'name',
    clearable: true,
    placeholder: "请输入数据名称",
  },
];
export const tableColumns = [

  {
    prop: 'type',
    label: '类型',
    overflow: true,
    isMultiCell: true,
    expandFunc: true,
    render: (scope) => {
      const status = scope.row.type;
      if (status === "0") {
        return "水表";
      } else if (status === "1") {
        return "电表";
      } else if (status === "2") {
        return "汽表";
      }
      return "--";
    }
  },
  {
    prop: 'status',
    label: '状态',
    overflow: true,
    isMultiCell: true,
    expandFunc: true,
    render: (scope) => {
      const status = scope.row.status;
      if (status === "0") {
        return "关闭";
      } else if (status === "1") {
        return "启动";
      }
      return "--";
    }
  },
  {
    prop: 'name',
    label: '数据名称',
    overflow: true
  },
  {
    prop: 'position',
    label: '位置',
    overflow: true
  },
  {
    prop: 'created_time',
    label: '创建时间',
    overflow: true
  },
  {
    prop: 'updated_time',
    label: '修改时间',
    overflow: true
  }
];

export const localService = {
  /**
   * {
   *  page: 1,
   *  psize: 20,
   *  params: {}
   * }
   */
  async get(data) {
    // eslint-disable-next-line @typescript-eslint/camelcase
    return await request.get("/dictionary/index", { page_no: data.page, page_size: data.psize, ...data.params }); // 这里是实际发请求的地方

  }
};

export const options = {
  canCheck: false, // 是否可选择
  hasIndex: true, // 是否有序号
  checkFixed: 'left', // 选择固定位置
  indexFixed: 'left', // 表序号固定位
  opW: 200,// 操作栏宽度
  autoRequest: true, // 自动请求
  startUpdate: Date.now()
};

// 以上配置文件可以根据业务需要分布配置在不同的文件里
