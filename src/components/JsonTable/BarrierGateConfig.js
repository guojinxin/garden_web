/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-12-07 10:03:05
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-14 14:52:38
 * @FilePath: \garden_web\src\components\JsonTable\useWaterConfig.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from "@/request";
import _ from "lodash";
// import { tableData } from './tableDataMock';
export const searchColumns = [
  {
    label: "人员姓名",
    prop: "user_name",
    clearable: true,
    placeholder: "请输入人员姓名",
  },
  {
    label: "进入时间段",
    prop: "inTime",
    clearable: true,
    placeholder: "选择进入时间段",
    isTime: "datetimerange",
  },
  {
    label: "离开时间段",
    prop: "outTime",
    clearable: true,
    placeholder: "选择离开时间段",
    isTime: "datetimerange",
  },
  {
    label: "部门",
    prop: "card_bz",
    clearable: true,
    placeholder: "请选择部门",
    isSelect: true,
    options: [
      {
        prop: "0",
        name: "研发部",
      },
    ],
  },
];

export const tableColumns = [
  {
    prop: "card_bz",
    label: "部门",
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      return scope.row.mFeeCard?.card_bz || "--";
    },
  },
  {
    prop: "user_name",
    label: "人员姓名",
    overflow: true,
  },
  {
    prop: "car_cp",
    label: "车牌号",
    overflow: true,
  },
  {
    prop: "in_time",
    label: "进入时间",
    overflow: true,
    render: (scope) => {
      return scope.row.in_time || "--";
    },
  },
  {
    prop: "out_time",
    label: "离开时间",
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      return scope.row?.out_time === 'Invalid date' ? "--"  : scope.row?.out_time;
    },
  },
];

export const localService = {
  /**
   * {
   *  page: 1,
   *  psize: 20,
   *  params: {}
   * }
   */
  get(data) {
    if (!_.isEmpty(data.params.inTime)) {

      // eslint-disable-next-line no-self-assign
      data.params.inTimeStr = data.params.inTime[0];
      // eslint-disable-next-line no-self-assign
      data.params.inTimeEnd = data.params.inTime[1];
      // delete data.params.dateVisit;
    } else {

      if (data.params.inTimeStr) {
        delete data.params.inTimeStr;
        delete data.params.inTimeEnd;
      }
    }
    if (!_.isEmpty(data.params.outTime)) {

      // eslint-disable-next-line no-self-assign
      data.params.outTimeStr = data.params.outTime[0];
      // eslint-disable-next-line no-self-assign
      data.params.outTimeEnd = data.params.outTime[1];
      // delete data.params.dateVisit;
    } else {
      if (data.params.outTimeStr) {

        delete data.params.outTimeStr;
        delete data.params.outTimeEnd;
      }
    }
    // eslint-disable-next-line @typescript-eslint/camelcase
    return request.get("/sluice/list", {page_no: data.page, page_size: data.psize,
      ...data.params,
    }); // 这里是实际发请求的地方
    // return new Promise((resolve, reject) => {
    //     setTimeout(() => {
    //         resolve(tableData);
    //     }, 1000);
    // });
  },
};

export const options = {
  canCheck: false, // 是否可选择
  hasIndex: true, // 是否有序号
  checkFixed: "left", // 选择固定位置
  indexFixed: "left", // 表序号固定位置
  opW: 200, // 操作栏宽度
  autoRequest: true, // 自动请求
  startUpdate: Date.now(),
};

// 以上配置文件可以根据业务需要分布配置在不同的文件里
