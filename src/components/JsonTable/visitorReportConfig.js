import request from '@/request';
import moment from 'moment';
import _ from 'lodash';
// import { tableData } from './tableDataMock';
export const searchColumns = [
  {
    label: '姓名',
    prop: 'name',
    clearable: true,
    placeholder: "请输入姓名"
  },
  {
    label: '申请人',
    prop: 'creator',
    clearable: true,
    placeholder: "请输入申请人姓名"
  },
  {
    label: '到访日期',
    prop: 'dateVisit',
    clearable: true,
    placeholder: "选择日期",
    isTime: 'datetimerange'
  },
  {
    label: '二维码',
    prop: 'id',
    clearable: true,
    placeholder: "请输入二维码链接"
  },
  {
    label: '状态',
    prop: 'status',
    clearable: true,
    placeholder: "状态",
    isSelect: true,
    options: [
      {
        prop: '0',
        name: '待审批'
      },
      {
        prop: '1',
        name: '已审批'
      },
      {
        prop: '2',
        name: '已接待'
      },
      {
        prop: '3',
        name: '驳回'
      },
      {
        prop: '4',
        name: '已到访'
      },
      {
        prop: '5',
        name: '已离开'
      }
    ]
  },
];

export const tableColumns = [
  {
    prop: 'createUser',
    label: '申请人',
    width: 150,
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      return scope?.row?.adminUser?.name || "--";
    }
  },
  {
    prop: 'name',
    label: '访客姓名',
    width: 200,
    overflow: true
  },
  {
    prop: 'visitTime',
    label: '拜访日期',
    width: 150,
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      if (scope.row.isContinuousVisit) {
      return moment(scope.row.visitStrTime).format('YYYY-MM-DD') + '~' + moment(scope.row.visitEndTime).format('YYYY-MM-DD') || "--";
      } else {
        return moment(scope.row.visitEndTime).format('YYYY-MM-DD') || "--";
      }
    }
  },
  {
    prop: 'address',
    label: '接访地点',
    width: 150,
    overflow: true
  },
  {   // 场景： 后端字段是数字0或1, 前端需要自己将数字转成汉字 比如0 通过 1 未通过
    prop: 'status',
    label: '状态',
    width: 150,
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      let status = scope.row.status;
      if (status === '0') { // 可编辑可删除
        return "待审批";
      } else if (status === '1') { // 可撤回 可查看二维码
        return "已审批";
      } else if (status === '2') {
        return '已接待';
      } else if (status === '3') { // 驳回，可编辑可删除
        return '待审批（驳回）';
      } else if (status === '4') { // 已接待按钮
        return '已到访';
      } else if (status === '5') {
        return '已离开';
      }
      return "--";
    }
  },
  {
    prop: 'licensePlateNum',
    label: '车牌号码',
    width: 150,
    overflow: true
  },
  {
    prop: 'visitUnit',
    label: '来访单位',
    width: 150,
    overflow: true
  },
  {   // 场景： 后端字段是json字符串，需要前端解析其中某个字段
    prop: 'describe',
    label: '拜访事由',
    width: 210,
    overflow: true,
  },
  {   // 图片预览
    prop: 'healthCode',
    label: '健康码',
    width: 150,
    imgW: 300, // 设置该项表示预览图片
    expandFunc: true // 是否有扩展功能，启用表格列插槽
  },

  {   // 图片预览
    prop: 'tourCode',
    label: '行程码',
    width: 150,
    imgW: 300, // 设置该项表示预览图片
    expandFunc: true // 是否有扩展功能，启用表格列插槽
  },
  {
    prop: 'idNum',
    label: '身份证号码',
    width: 150,
    overflow: true
  },
  {
    prop: 'phone',
    label: '手机号码',
    width: 150,
    overflow: true
  },
  {
    prop: 'applyDepartment',
    label: '申请部门',
    width: 150,
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      return scope?.row?.adminUser?.department || "--";
    }
  },
  {
    prop: 'auditUser',
    label: '审核人',
    width: 150,
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      return scope.row.approverUser?.name || "--";
    }
  },
  {
    prop: 'jobNumber',
    label: '申请人工号',
    width: 150,
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      return scope?.row?.adminUser?.jobNum || "--";
    }
  },
  {
    prop: 'created_time',
    label: '创建时间',
    width: 150,
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      return moment(scope?.row?.created_time).format('YYYY-MM-DD HH:mm:ss');
    }
  },
  // {
  //   prop: 'estimatedStrTime',
  //   label: '预计到达时间',
  //   width: 150,
  //   overflow: true,
  //   expandFunc: true,
  //   isMultiCell: true,
  //   render: (scope) => {
  //     return scope?.row?.estimatedStrTime ? moment(scope?.row?.estimatedStrTime).format('YYYY-MM-DD hh:mm:ss') : '-';
  //   }
  // },
  // {
  //   prop: 'estimatedEndTime',
  //   label: '预计结束时间',
  //   width: 150,
  //   overflow: true,
  //   expandFunc: true,
  //   isMultiCell: true,
  //   render: (scope) => {
  //     return scope?.row?.estimatedEndTime ? moment(scope?.row?.estimatedEndTime).format('YYYY-MM-DD hh:mm:ss') : '-';
  //   }
  // },
  {
    prop: 'remark',
    label: '备注',
    width: 150,
    overflow: true
  },
];

export const localService = {
  /**
   * {
   *  page: 1,
   *  psize: 20,
   *  params: {}
   * }
   */
  get(data) {

    if (!_.isEmpty(data.params.dateVisit)) {
      // eslint-disable-next-line no-self-assign
      data.params.dateVisitStr = data.params.dateVisit[0];
      // eslint-disable-next-line no-self-assign
      data.params.dateVisitEnd = data.params.dateVisit[1];
      // delete data.params.dateVisit;
    } else {
      delete data.params.dateVisitStr;
      delete data.params.dateVisitEnd;
    }
    // eslint-disable-next-line @typescript-eslint/camelcase
    return request.get("visit/visitorReport", { page_no: data.page, page_size: data.psize, ...data.params }); // 这里是实际发请求的地方
    // return new Promise((resolve, reject) => {
    //     setTimeout(() => {
    //         resolve(tableData);
    //     }, 1000);
    // });
  }
};

export const options = {
  canCheck: false, // 是否可选择
  hasIndex: true, // 是否有序号
  checkFixed: 'left', // 选择固定位置
  indexFixed: 'left', // 表序号固定位置
  opW: 200,// 操作栏宽度
  autoRequest: true, // 自动请求
  startUpdate: Date.now()
};

// 以上配置文件可以根据业务需要分布配置在不同的文件里
