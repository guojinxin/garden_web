/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-15 16:19:23
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-04-26 09:40:30
 * @FilePath: \garden_web\src\components\JsonTable\userConfig.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from '@/request';
export const searchColumns = [
  {
    label: '设备名称',
    prop: 'ciName',
    clearable: true,
    placeholder: "请输入设备名称",
  },
  {
    label: '告警级别',
    prop: 'alarmlevel',
    clearable: true,
    placeholder: "告警级别",
    isSelect: true,
    options: [
      {
        prop: '1',
        name: '严重',
      },
      {
        prop: '2',
        name: '重要',
      },
      {
        prop: '3',
        name: '次要',
      },
      {
        prop: '4',
        name: '普通',
      }
    ]
  },
  {
    label: '状态',
    prop: 'status',
    clearable: true,
    placeholder: "状态",
    isSelect: true,
    options: [
      {
        prop: '1',
        name: '待处理',
      },
      {
        prop: '2',
        name: '处理中',
      },
      {
        prop: '3',
        name: '关闭',
      }
    ]
  },
];
export const tableColumns = [
  {
    prop: 'ciName',
    label: '设备名称',
    overflow: true
  },
  {
    prop: 'alarmlevel',
    label: '告警级别',
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      let status = scope.row.alarmlevel;
      if (status === "1") {
        return "严重";
      } else if (status === "2") {
        return "重要";
      } else if (status === "3") {
        return '次要';
      } else if (status === "4") {
        return '普通';
      }
      return "--";
    }
  },
  {
    prop: 'status',
    label: '状态',
    overflow: true,
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
      let status = scope.row.status;
      if (status === "1") {
        return "待处理";
      } else if (status === "2") {
        return "处理中";
      } else if (status === "3") {
        return '关闭';
      }
      return "--";
    }
  },
  {
    prop: 'handlingOpinions',
    label: '处理意见',
    overflow: true
  },
  {
    prop: 'alarm_time',
    label: '告警时间',
    overflow: true
  },
  {
    prop: 'modify_time',
    label: '修改时间',
    overflow: true,

  },
  {
    prop: 'alarm_information',
    label: '告警描述',
    overflow: false,
  },
  {
    prop: 'name',
    label: '处理人姓名',
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
        return scope.row.adminUser?.name || "--";
    }
  },
  {
    prop: 'phone',
    label: '处理人手机号',
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
        return scope.row.adminUser?.phone || "--";
    }
  },
  {
    prop: 'email',
    label: '处理人邮箱',
    expandFunc: true,
    isMultiCell: true,
    render: (scope) => {
        return scope.row.adminUser?.email || "--";
    }
  },
];

export const localService = {
  /**
   * {
   *  page: 1,
   *  psize: 20,
   *  params: {}
   * }
   */
  async get(data) {
    // eslint-disable-next-line @typescript-eslint/camelcase
    return await request.get("alarm/index", { page_no: data.page, page_size: data.psize, ...data.params }); // 这里是实际发请求的地方

  }
};

export const options = {
  canCheck: false, // 是否可选择
  hasIndex: true, // 是否有序号
  checkFixed: 'left', // 选择固定位置
  indexFixed: 'left', // 表序号固定位
  opW: 200,// 操作栏宽度
  autoRequest: true, // 自动请求
  startUpdate: Date.now()
};

// 以上配置文件可以根据业务需要分布配置在不同的文件里
