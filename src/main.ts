/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-10 13:24:23
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-03-02 14:38:12
 * @FilePath: \Vue-Onepiece-Admin-master\src\main.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { createApp, ref } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import request from "@/request/index";
import zhLocale from 'element-plus/lib/locale/lang/zh-cn';
import './font-icon/iconfont.css';

import 'dayjs/locale/zh-cn';
(ElementPlus as any).useLang = (app: { provide: (arg0: string, arg1: { lang: any; locale: any; t: (...args: any[]) => any }) => void }, ref: (arg0: any) => any, locale: { name: any }) => {
  const template = (str: string, option: { [x: string]: any }) => {
    if (!str || !option) return str;
    return str.replace(/\{(\w+)\}/g, (_, key) => {
      return option[key];
    });
  };

  // 注入全局属性,子组件都能通过inject获取
  app.provide('ElLocaleInjection', {
    lang: ref(locale.name),
    locale: ref(locale),
    t: (...args) => {
      const [path, option] = args;
      let value;
      const array = path.split('.');
      let current: any = locale;
      for (let i = 0, j = array.length; i < j; i++) {
        const property = array[i];
        value = current[property];
        if (i === j - 1) return template(value, option);
        if (!value) return '';
        current = value;
      }
    },
  });
};

// 这里监听请求的错误统一处理（做弹窗提示提示）
request.on("HttpStatusFaild", () => {
  alert("请求失败，请检查接口问题");
});
const app = createApp(App);

(ElementPlus as any).useLang(app, ref, zhLocale);

const result = window.matchMedia("(min-width: 400px)").matches;

if (result) {
  app.use(store).use(router).use(ElementPlus, { zhLocale }).mount('#app');
} else {
  app.use(store).use(router).use(ElementPlus, { zhLocale }).mount('#app');
}
