/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-10 17:40:50
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-09-26 14:02:48
 * @FilePath: \garden_web\src\router\defaultRoutes\index.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Inner from "@/views/Inner.vue";
import NotFound from '@/views/NotFound.vue';
import Login from "@/views/Login.vue";
import UserDetailsPage from "@/views/UserDetailsPage.vue";

const defaultRoutes: any = [
  {
    path: "/inner",
    name: "内部页面",
    component: Inner,
    meta: {
      activePath: '/'  // 打开非Menu页面选择当前激活menu
    }
  },
  {
    path: '/:pathMatch(.*)*',
    name: '404',
    component: NotFound
  },
  {
    path: '/login',
    name: '登录',
    component: Login
  },
  {
    path: '/userDetails/:id/:visitId',
    name: '访客信息',
    component: UserDetailsPage
  }
];

export default defaultRoutes;
