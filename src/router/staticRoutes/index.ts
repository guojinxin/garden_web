/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-10 17:40:50
 * @LastEditors: guojinxin_hub 1907745233@qq.com
 * @LastEditTime: 2023-06-30 17:38:56
 * @FilePath: \garden_web\src\router\staticRoutes\index.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import User from "@/views/User.vue";
import Component from "@/views/Component.vue";
import WorkOrderManagement from "@/views/WorkOrderManagement.vue";
import DictionaryManagement from "@/views/DictionaryManagement.vue";
import _ from "lodash";
import EnergyConsumption from "@/views/EnergyConsumption.vue";
import SteamConsumption from "@/views/SteamConsumption.vue";
import ElectricityConsumption from "@/views/ElectricityConsumption.vue";
import EnergyMonthConsumption from "@/views/EnergyMonthConsumption.vue";
import PersonalCenter from "@/views/PersonalCenter.vue";
import AttendancePage from "@/views/AttendancePage.vue";
import BarrierGatePage from "@/views/BarrierGatePage.vue";
import AccessControlPage from "@/views/AccessControlPage.vue";
import GuardTourReportPage from "@/views/GuardTourReportPage.vue";
import VisitorReportPage from '@/views/VisitorReportPage.vue';
/**
 *
 * 路由配置规则：
 *
 * {
 *  path:'',路径
 *  name:'',路由名称，生成menu时menu name
 *  meta:{},额外信息，icon为menu中的icon
 *  children: [], 子路由，menu中的子menu 没有时可为空数组
 * }
 *
 */

const userInfo = JSON.parse(localStorage.getItem("user") || JSON.stringify(""));
const routerList: any = [
  {
    path: userInfo?.role === "2" ? "/component" : "/",
    name: "访客管理",
    component: Component,
    children: [],
    meta: {
      icon: "el-icon-fangkeguanli",
    },
  },
];
if (userInfo?.role === "2" || userInfo?.isAlarm) {
  routerList.push({
    path: "/workOrderManagement",
    name: "告警工单管理",
    component: WorkOrderManagement,
    children: [],
    meta: {
      icon: "el-icon-gaojingguanli",
    },
  });
}
if (userInfo?.role === "2" || !_.isEmpty(userInfo?.operationPermission)) {
  const childrenList = [];
  const statementList = [];
  if (userInfo?.role === "2") {
    statementList.push(
      {
        path: "attendanceReport",
        name: "考勤报表",
        meta: {
          icon: "el-icon-kaoqinguanli",
        },
        component: AttendancePage,
        children: [],
      },
      {
        path: "barrierGate",
        name: "道闸报表",
        meta: {
          icon: "el-icon-chedaozha",
        },
        component: BarrierGatePage,
        children: [],
      },
      {
        path: "accessControl",
        name: "门禁报表",
        meta: {
          icon: "el-icon-menjinguanli",
        },
        component: AccessControlPage,
        children: [],
      },
      {
        path: "guardTourReport",
        name: "巡更报表",
        meta: {
          icon: "el-icon-xungeng",
        },
        component: GuardTourReportPage,
        children: [],
      },
      {
        path: "visitorReport",
        name: "访客报表",
        meta: {
          icon: "el-icon-fangkebaobiao",
        },
        component:  VisitorReportPage,
        children: [],
      }
    );
    childrenList.push(
      {
        path: "water",
        name: "用水情况",
        meta: {
          icon: "el-icon-yongshui",
        },
        component: EnergyConsumption,
        children: [],
      },
      {
        path: "electricity",
        name: "用电情况",
        meta: {
          icon: "el-icon-shebeiyongdianjiebo",
        },
        component: ElectricityConsumption,
        children: [],
      },
      {
        path: "steam",
        name: "用汽情况",
        meta: {
          icon: "el-icon-yongqiliang",
        },
        component: SteamConsumption,
        children: [],
      },
      {
        path: "energyMonth",
        name: "月度能耗使用情况",
        meta: {
          icon: "el-icon-nengyuannenghaoguanli",
        },
        component: EnergyMonthConsumption,
        children: [],
      }
    );
  } else {
    const isMonthlyEnergy = userInfo?.operationPermission.find(
      (item: string) => {
        return item === "月度能耗使用情况";
      }
    );
    if (!_.isEmpty(isMonthlyEnergy)) {
      childrenList.push({
        path: "energyMonth",
        name: "月度能耗使用情况",
        meta: {
          icon: "el-icon-nengyuannenghaoguanli",
        },
        component: EnergyMonthConsumption,
        children: [],
      });
    }
    const isAttendance = userInfo?.operationPermission.find((item: string) => {
      return item === "考勤报表";
    });
    if (!_.isEmpty(isAttendance)) {
      statementList.push({
        path: "attendanceReport",
        name: "考勤报表",
        meta: {
          icon: "el-icon-kaoqinguanli",
        },
        component: AttendancePage,
        children: [],
      });
    }
    const isBarrierGate = userInfo?.operationPermission.find((item: string) => {
      return item === "道闸报表";
    });
    if (!_.isEmpty(isBarrierGate)) {
      statementList.push({
        path: "barrierGate",
        name: "道闸报表",
        meta: {
          icon: "el-icon-chedaozha",
        },
        component: BarrierGatePage,
        children: [],
      });
    }
    const isAccessControl = userInfo?.operationPermission.find(
      (item: string) => {
        return item === "门禁报表";
      }
    );
    if (!_.isEmpty(isAccessControl)) {
      statementList.push({
        path: "accessControl",
        name: "门禁报表",
        meta: {
          icon: "el-icon-menjinguanli",
        },
        component: AccessControlPage,
        children: [],
      });
    }
    const isGuardTourReport = userInfo?.operationPermission.find(
      (item: string) => {
        return item === "巡更报表";
      }
    );
    if (!_.isEmpty(isGuardTourReport)) {
      statementList.push({
        path: "guardTourReport",
        name: "巡更报表",
        meta: {
          icon: "el-icon-xungeng",
        },
        component: GuardTourReportPage,
        children: [],
      });
    }
    const isVisitorReport = userInfo?.operationPermission.find(
      (item: string) => {
        return item === "访客报表";
      }
    );
    if (!_.isEmpty(isVisitorReport)) {
      statementList.push({
        path: "visitorReport",
        name: "访客报表",
        meta: {
          icon: "el-icon-fangkebaobiao",
        },
        component:  VisitorReportPage,
        children: [],
      });
    }

    const isWater = userInfo?.operationPermission.find((item: string) => {
      return item === "水表管控";
    });

    if (!_.isEmpty(isWater)) {
      childrenList.push({
        path: "water",
        name: "用水情况",
        meta: {
          icon: "el-icon-yongshui",
        },
        component: EnergyConsumption,
        children: [],
      });
    }
    const isElectricity = userInfo?.operationPermission.find((item: string) => {
      return item === "电表管控";
    });

    if (!_.isEmpty(isElectricity)) {
      childrenList.push({
        path: "electricity",
        name: "用电情况",
        meta: {
          icon: "el-icon-shebeiyongdianjiebo",
        },
        component: ElectricityConsumption,
        children: [],
      });
    }
    const isDictionary = userInfo?.operationPermission.find(
      (item: string) => {
        return item === "字典管理";
      }
    );
    if (!_.isEmpty(isDictionary)) {
      routerList.push({
        path: "/dictionaryManagement",
        name: "字典管理",
        component: DictionaryManagement,
        children: [],
        meta: {
          icon: "el-icon-zidianguanli",
        },
      });
    }
    const isSteam = userInfo?.operationPermission.find((item: string) => {
      return item === "汽表管控";
    });
    if (!_.isEmpty(isSteam)) {
      childrenList.push({
        path: "steam",
        name: "用汽情况",
        meta: {
          icon: "el-icon-yongqiliang",
        },
        component: SteamConsumption,
        children: [],
      });
    }
  }
  if (!_.isEmpty(statementList)) {
    routerList.push({
      path: "/reportManager",
      name: "报表管理",
      redirect: "/reportManager/attendanceReport",
      meta: {
        icon: "el-icon-baobiao",
      },
      children: statementList,
    });
  }
  if (!_.isEmpty(childrenList)) {
    routerList.push({
      path: "/energyConsumption",
      name: "能耗管理",
      redirect: "/energyConsumption/water",
      meta: {
        icon: "el-icon-nengyuannenghaoguanli",
      },
      children: childrenList,
    });
  }
}
if (userInfo?.role === "2") {
  routerList.push(
    {
      path: "/dictionaryManagement",
      name: "字典管理",
      component: DictionaryManagement,
      children: [],
      meta: {
        icon: "el-icon-zidianguanli",
      },
    },
    {
      path: "/",
      name: "成员管理",
      component: User,
      children: [],
      meta: {
        icon: "el-icon-chengyuanguanli",
      },
    }
  );
}
routerList.push({
  path: "/personalCenter",
  name: "个人中心",
  component: PersonalCenter,
  children: [],
  meta: {
    icon: "el-icon-coordinate",
  },
});
export const staticRoutes = routerList;
