/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-19 09:39:46
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-08-19 09:41:21
 * @FilePath: \garden_web\src\auth\index.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
export function common(rule: any, value: any, callback: any, reg: any, emptymsg: any, errormsg: any) {

    if (value === '' || value === undefined) {
        // defRequired 是自定义的属性，在表单必须填写但是label标签前面没有*（星号）
        if (rule.required || rule.defRequired) {
            callback(new Error(emptymsg));
        } else {
            callback();
        }

    } else if (reg.test(value)) {
        callback();
    } else {
        callback(new Error(errormsg));
    }

}