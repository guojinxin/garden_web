/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-31 16:04:45
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2022-08-31 16:04:52
 * @FilePath: \garden_web\src\utils\mitt.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import mitt from 'mitt';
const emitter = mitt();
export default emitter;