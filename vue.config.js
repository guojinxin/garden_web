/*
 * @Author: guojinxin 1907745233@qq.com
 * @Date: 2022-08-10 17:40:50
 * @LastEditors: guojinxin 1907745233@qq.com
 * @LastEditTime: 2023-03-01 09:51:13
 * @FilePath: \garden_web\vue.config.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
module.exports = {
    publicPath: '/',
    devServer: {
        open: true,
        host: 'localhost',
        port: 8000,
        https: false,
        //以上的ip和端口是我们本机的;下面为需要跨域的
        proxy: {  //配置跨域
            '/api': {
                target: 'http://10.0.0.21:7001/',  //这里后台的地址模拟的;应该填写你们真实的后台接口
                ws: true,
                changOrigin: true,  //允许跨域
                pathRewrite: {
                    '^/api': ''  //请求的时候使用这个api就可以
                }
            }
        }
    },
    configureWebpack: {
        devtool: "inline-source-map"
    },
    css: {
        extract: false
    }
};
